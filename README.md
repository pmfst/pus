# PuS

Pomoćni materijali za nastavu iz kolegija Programiranje u struci na Prirodoslovno-matematičkom fakultetu Sveučilišta u Splitu.

Kolegij se već više godina izvodi u Python programskom jeziku. Stoga je većina nastavnog materijala dana kao jupyter notebook koji se jednostavno instalira.

Quick install (Linux):

    python3 -m venv PUS
    source PUS/bin/activate
    pip install jupyter #imena_drugih_paketa

Quick install (Win):

    conda create --name pus python=3.9
    conda install --name pus jupyter #imena_drugih_paketa
    activate pus

Više detalja možete naći u tj01/v1.pdf

Datoteke su imenovane po ključu `tj*` koji označava tjedan nastave, odnosno .ipynb datoteke po ključu `p*.ipynb`, odnosno `v*.ipynb` koji označavaju predavanja i vježbe. U nekim slučajevima pridodani su dodatni materijali - primjerice `p*_dodatno.ipynb`, dodatne .py datoteke ili čitavi direktorij (tipa `dz`). Kada se u direktoriju nalazi datoteka `*_dodatno.ipynb` najčešće je riječ o kopilaciji različitih pitanja ili diskusija koje su se pojavile u prethodnim generacijama, ili (alternativnom) rješenju nekog zadatka. Svakoj generaciji savjetujem da pogleda i taj dio - ali tek nakon što pokuša samostalno riješiti zadatke tog tjedna.

Materijali su posloženi (približno) po tjednima nastave i namijenjeni su studentima Fakulteta, odnosno njihovoj pripremi za ispit. Za druge stvari se ne smiju koristiti bez dozvole autora.


