# py4C

Materijali u ovom direktoriju su nastali kao pomoćni materijali za kolegij Računalna inteligencija s primjenama. Naime, u trenutku nastajanja tog kolegija predmet Programiranje u struci (1 i 2) se još uvijek izvodio u C programskom jeziku. Stoga su nastali ovi materijali da se olakša prijelaz sa C-oidnog načina razmišljanja na Python, prije nego se upoznamo s Pandas-om, sklearnom i neuronskim mrežema (inicijalno Tensorflow, kasnije PyTorch).

Neki primjeri su preslika primjera iz C-a, radi lakše usporedbe algoritama.


